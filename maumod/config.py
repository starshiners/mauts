
from mautrix.util.config import BaseProxyConfig, ConfigUpdateHelper

class Config(BaseProxyConfig):
    def do_update(self, helper: ConfigUpdateHelper) -> None:
        helper.copy('chat_command')
        helper.copy('complete_command')
        helper.copy('translate_command')
        helper.copy('continue_command')
        helper.copy('config_command')
        helper.copy('clear_command')
        helper.copy('help_command')
        helper.copy('cleanup_delta')
        helper.copy('chat_history')

        helper.copy('chat_engine')
        helper.copy('completion_engine')
        helper.copy('question_engine')
        helper.copy('translation_engine')
        helper.copy('max_tokens')
        helper.copy('top_k')
        helper.copy('top_p')