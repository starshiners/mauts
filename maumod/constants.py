TS_URL = "https://api.textsynth.com/v1/engines"

ENGINES_COMPLETION = [
    "llama3_8B",
    "mistral_7B",
    "llama2_70B",
]
ENGINES_CHAT = [
    "llama3_8B_instruct",
    "mixtral_47B_instruct",
]
ENGINES_TRANSLATION = ["madlad400_7B"]
ENGINES_QUESTION = [
    "flan_t5_xxl",
]
ENGINES_GENERATE = ["stable_diffusion"]

TOP_P_RANGE = range(1, 1001)
TOP_K_RANGE = range(0, 11)

MAX_TOKENS_RANGE = range(50, 1001)

LANGUAGE_CODES = {
    "af": "Afrikaans",
    "sq": "Albanian",
    "am": "Amharic",
    "ar": "Arabic",
    "hy": "Armenian",
    "ast": "Asturian",
    "az": "Azerbaijani",
    "ba": "Bashkir",
    "be": "Belarusian",
    "bn": "Bengali",
    "bs": "Bosnian",
    "bd": "Bulgarian",
    "my": "Burmese",
    "ca": "Catalan",
    "ceb": "Cebuana",
    "km": "Certal Khmer",
    "zh": "Chinese",
    "he": "Croatian",
    "cs": "Czech",
    "da": "Danish",
    "nl": "Dutch",
    "en": "English",
    "et": "Estonian",
    "fi": "Finnish",
    "fr": "French",
    "ff": "Fulah",
    "gl": "Galician",
    "lg": "Ganda",
    "ka": "Georgian",
    "de": "German",
    "el": "Greek",
    "gu": "Gujarati",
    "ht": "Haitian Creole",
    "ha": "Hausa",
    "he": "Hebrew",
    "hi": "Hindi",
    "hu": "Hungarian",
    "is": "Icelandic",
    "ig": "Igbo",
    "ilo": "Iloko",
    "id": "Indonesian",
    "ga": "Irish",
    "kn": "Kannada",
    "kk": "Kazakh",
    "ko": "Korean",
    "lo": "Lao",
    "lv": "Latvian",
    "ln": "Lingala",
    "lt": "Lithuanian",
    "lb": "Luxembourgish",
    "mk": "Macedonian",
    "mg": "Malagasy",
    "ms": "Malay",
    "ml": "Maylayalam",
    "mr": "Marathi",
    "mn": "Mongolian",
    "ne": "Nepali",
    "nso": "Southern Sotho",
    "no": "Norwegian",
    "oc": "Occitan",
    "or": "Oriya",
    "pa": "Punjabi",
    "ps": "Pashto",
    "fa": "Persian",
    "pl": "Polish",
    "pt": "Portuguese",
    "ro": "Romanian",
    "ru": "Russian",
    "gd": "Scottish Gaelic",
    "sr": "Serbian",
    "sd": "Sindhi",
    "si": "Sinhala",
    "sk": "Slobak",
    "su": "Sudanese",
    "sw": "Swahili",
    "ss": "Swati",
    "sv": "Swedish",
    "tl": "Tagalog",
    "ta": "Tamil",
    "th": "Thai",
    "tn": "Tswana",
    "tr": "Turkish",
    "uk": "Ukranian",
    "ur": "Urdu",
    "uz": "Uzbek",
    "vi": "Vietnamese",
    "cy": "Welsch",
    "fy": "Western Frisian",
    "wo": "Wolof",
    "xh": "Xhosa",
    "yi": "Yiddish",
    "yo": "Yoruba",
    "zu": "Zulu",
}

CHECK = "✅"
CROSS = "❌"
END = "🔚"

HELP_TEXTS = {
    "tschat": (
        f"!tschat: <message>"
        f"\nChat with your chosen model."
        f"\nThe previous 10 messages are retained for a configurable time"
        f" in order to provide context."
    ),
    "tscomp": (
        f"!tscomp <message>"
        f"\nComplete the text using your chosen model."
        f"\nThe previous 10 messages are retained for a configurable time"
        f" in order to provide context."
    ),
    "trans": (
        f"!trans <from_lang> <to_lang> <message>"
        f"\n<from_lang> is any accepted language code"
        f"\n<to_lang> is any accepted langauge code"
        f"\n<message> a string of arbitrary length to translate"
        f"\nYou can get a list of available languages with !tslang"
    ),
    "tscont": (
        f"!tscont [chat | complete]"
        f"\nArgument must be `chat` or `complete`"
        f"\nIf chat, will request further generation of the bots last chat response"
        f" -- given you have an active history."
        f"\nIf complete, will request further generation of your last complete"
        f" response -- given you have an active history."
    ),
    "tsgen": (
        f"!tsgen <prompt>"
        f"\nGenerate one or more images based on the prompt given."
        f"\nOptional parameters:"
        f"\n  count=<num>"
        f"\n  timesteps=<num>"
        f"\n  guidance_scale=<float>"
        f"\n  neg=<neg prompt> (add this at end)"
    ),
    "tsconf": (
        f"!tsconf [<key> [<value>]]"
        f"\nIn no argument is given, print your current configuration."
        f"\nIf a valid key is specified, return the current value and available options."
        f"\nIf both a valid key and value are specified, set the current key to that value."
    ),
    "tsclear": (
        f"!tsclear"
        f"\nWill clear your active chat histories -- if any are present."
        f"\nIf you do not use this command, they will be wiped in a configurable"
        f" time anyway. (Default 600 seconds.)"
    ),
    "tspurge": (
        f"!tspurge"
        f"\nWill delete your user configuration information from the database."
        f"\nPlease note if you use any further commands of this bot they will be regenerated."
    ),
    "compliment": (
        f"!compliment [subscribe | unsubscribe]"
        f"\nEither subscribe or unsubscribe from a daily compliment in this channel."
        f"\nNote you can only have one active subscription."
    ),
}

HELP_SUMMARY = (
    f"Available Commands:"
    f"\n* !tschat <message>"
    f"\n* !tscomp <message>"
    f"\n* !trans <from_lang> <to_lang> <message>"
    f"\n* !tslang Get available translation langues."
    f"\n* !tsgen <prompt>"
    f"\n* !tscont [chat | complete]"
    f"\n* !tsconf [<key> [<value>]]"
    f"\n* !tsclear Clear chat history"
    f"\n* !tspurge Purge your info from database"
    f"\n* !compliment [subscribe | unsubscribe]"
)

CONFIG_HELP_TEXTS = {
    "chat_engine": ("\n".join([f"* `{x}`" for x in ENGINES_CHAT])),
    "completion_engine": ("\n".join([f"* `{x}`" for x in ENGINES_COMPLETION])),
    "question_engine": ("\n".join([f"* `{x}`" for x in ENGINES_QUESTION])),
    "translation_engine": ("\n".join([f"* `{x}`" for x in ENGINES_TRANSLATION])),
    "max_tokens": (
        f"Max tokens must be in range {MAX_TOKENS_RANGE[0]} - {MAX_TOKENS_RANGE[-1]}"
    ),
    "top_k": (f"top_k must be in range {TOP_K_RANGE[0]} - {TOP_K_RANGE[-1]}"),
    "top_p": (
        f"top_p must be in range {TOP_P_RANGE[0]} - {TOP_P_RANGE[-1]}"
        f"\nNote this number the number you provide is internally divided by 10."
    ),
}

ADJECTIVES = [
    "Accepting",
    "Active",
    "Adaptable",
    "Adventurous",
    "Affectionate",
    "Alert",
    "Ambitious",
    "Amiable",
    "Amusing",
    "Assertive",
    "Attentive",
    "Brave",
    "Brilliant",
    "Broad",
    "Calm",
    "Candid",
    "Careful",
    "Charming",
    "Cheerful",
    "Clever",
    "Communicative",
    "Confident",
    "Compassionate",
    "Conscientious",
    "Considerate",
    "Courageous",
    "Courteous",
    "Creative",
    "Curious",
    "Decisive",
    "Dependable",
    "Determined",
    "Diligent",
    "Diplomatic",
    "Disciplined",
    "Discreet",
    "Dynamic",
    "Easygoing",
    "Efficient",
    "Energetic",
    "Enthusiastic",
    "Exuberant",
    "Fairminded",
    "Faithful",
    "Fearless",
    "Forceful",
    "Forgiving",
    "Frank",
    "Friendly",
    "Generous",
    "Genuine",
    "Gentle",
    "Graceful",
    "Gregarious",
    "Hard",
    "Helpful",
    "Honest",
    "Hopeful",
    "Humble",
    "Imaginative",
    "Impartial",
    "Independent",
    "Intelligent",
    "Intuitive",
    "Innovative",
    "Just",
    "Kind",
    "Liberal",
    "Loving",
    "Loyal",
    "Mature",
    "Merciful",
    "Meticulous",
    "Modest",
    "Neat",
    "Optimistic",
    "Organized",
    "Passionate",
    "Patient",
    "Persistent",
    "Pioneering",
    "Philosophical",
    "Placid",
    "Playful",
    "Plucky",
    "Polite",
    "Powerful",
    "Practical",
    "Proactive",
    "Punctual",
    "Purposeful",
    "Quick",
    "Quiet",
    "Rational",
    "Reliable",
    "Reserved",
    "Resolute",
    "Resourceful",
    "Romantic",
    "Self",
    "Sensible",
    "Sensitive",
    "Sincere",
    "Skillful",
    "Smart",
    "Sociable",
    "Straightforward",
    "Strong",
    "Supportive",
    "Sympathetic",
    "Tenacious",
    "Tactful",
    "Thoughtful",
    "Tidy",
    "Tolerant",
    "Tough",
    "Trustworthy",
    "Unassuming",
    "Understanding",
    "Versatile",
    "Warmhearted",
    "Wise",
    "Witty",
]
