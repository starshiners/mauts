
from typing import Union
from attr import dataclass
from datetime import datetime

from asyncpg import Record
from mautrix.util.async_db import Connection, Database, Scheme, UpgradeTable
from mautrix.types import UserID, RoomID
from mautrix.util.config import BaseProxyConfig
from mautrix.util.async_db import UpgradeTable, Scheme
from mautrix.util.logging import TraceLogger


@dataclass
class UserInfo:
    chat_engine: str = None
    completion_engine: str = None
    question_engine: str = None
    translation_engine: str = None
    max_tokens: str = None
    top_k: int = None
    top_p: int = None

    def __str__(self) -> str:
        return (
            f'Your information:'
            f'\n* **chat_engine:** {self.chat_engine}'
            f'\n* **completion_engine:** {self.completion_engine}'
            f'\n* **question_engine:** {self.question_engine}'
            f'\n* **translation_engine:** {self.translation_engine}'
            f'\n* **max_tokens:** {self.max_tokens}'
            f'\n* **top_k:** {self.top_k}'
            f'\n* **top_p:** {self.top_p}'
        )


upgrade_table = UpgradeTable()

@upgrade_table.register(description='Initial revision')
async def upgrade_v1(conn: Connection, scheme: Scheme):
    await conn.execute(
        """CREATE TABLE user_config (
            id INTEGER PRIMARY KEY,
            user_id varchar(255) NOT NULL,
            chat_engine varchar(255) NOT NULL,
            completion_engine varchar(255) NOT NULL,
            question_engine varchar(255) NOT NULL,
            translation_engine varchar(255) NOT NULL,
            max_tokens INTEGER NOT NULL,
            top_k INTEGER NOT NULL,
            top_p INTEGER NOT NULL
        )"""
    )

@upgrade_table.register(description='Revision 2')
async def upgrade_v2(conn: Connection, scheme: Scheme):
    await conn.execute("""DROP TABLE IF EXISTS user_config""")
    await conn.execute(
        """CREATE TABLE user_config (
            id INTEGER GENERATED ALWAYS AS IDENTITY,
            user_id varchar(255) NOT NULL,
            chat_engine varchar(255) NOT NULL,
            completion_engine varchar(255) NOT NULL,
            question_engine varchar(255) NOT NULL,
            translation_engine varchar(255) NOT NULL,
            max_tokens INTEGER NOT NULL,
            top_k INTEGER NOT NULL,
            top_p INTEGER NOT NULL,

            PRIMARY KEY (id)
        )"""
    )

@upgrade_table.register(description='Revision 3')
async def upgrade_v3(conn: Connection, scheme: Scheme):
    await conn.execute(
        """CREATE TABLE compliments (
            id INTEGER GENERATED ALWAYS AS IDENTITY,
            user_id varchar(255) NOT NULL,
            room_id varchar(255) NOT NULL,
            subscribe_time timestamp NOT NULL,

            PRIMARY KEY (id)
        )
        """
    )


class TSDB:
    db: Database
    config: BaseProxyConfig
    log: TraceLogger

    def __init__(self, db: Database, config: BaseProxyConfig, log: TraceLogger) -> None:
        self.db = db
        self.config = config
        self.log = log

    async def get_user_config(self, user_id: UserID) -> UserInfo:
        res = await self._try_get_user(user_id)
        if res:
            return res
        else:
            await self._make_defalt_config(user_id)
            res = await self._try_get_user(user_id)
            return res

    async def _try_get_user(self, user_id: UserID) -> Union[bool, UserInfo]:
        query = """
        SELECT chat_engine, completion_engine, question_engine,
            translation_engine, max_tokens, top_k, top_p
        FROM user_config
        WHERE user_id = $1
        """
        row = await self.db.fetchrow(query, user_id)
        if row:
            return UserInfo(
                chat_engine=row[0],
                completion_engine=row[1],
                question_engine=row[2],
                translation_engine=row[3],
                max_tokens=row[4],
                top_k=row[5],
                top_p=row[6]
            )
        else:
            return False

    async def _make_defalt_config(self, user_id: UserID) -> None:
        query = """
        INSERT INTO user_config (user_id, chat_engine, completion_engine,
            question_engine, translation_engine, max_tokens, top_p, top_k)
        VALUES ($1, $2, $3, $4, $5, $6, $7, $8)
        """
        await self.db.execute(query,
                              user_id,
                              self.config['chat_engine'],
                              self.config['completion_engine'],
                              self.config['question_engine'],
                              self.config['translation_engine'],
                              self.config['max_tokens'],
                              self.config['top_p'],
                              self.config['top_k'])

    async def purge_user(self, user_id: UserID) -> None:
        query = """
        DELETE FROM user_config
        WHERE user_id = $1
        """
        await self.db.execute(query, user_id)

    async def set_value(self, user_id: UserID, key: str, val: Union[str, int]) -> None:
        query = f"""
        UPDATE user_config
        SET {key} = $1
        WHERE user_id = $2
        """
        await self.db.execute(query, val, user_id)

    async def subscribe_user(self, user_id: str, room_id: RoomID) -> None:
        query = """INSERT INTO compliments (user_id, room_id, subscribe_time)
        VALUES ($1, $2, $3)
        """
        await self.unsubscribe_user(user_id)
        await self.db.execute(query, user_id, room_id, datetime.now())

    async def unsubscribe_user(self, user_id: str) -> None:
        query = """DELETE FROM compliments
        WHERE user_id = $1
        """
        await self.db.execute(query, user_id)

    async def get_subscriptions(self) -> list[Record]:
        query = """SELECT user_id, room_id, subscribe_time from compliments"""
        return await self.db.fetch(query)
