"""
<A maubot plugin for the textsynth API.>
Copyright (C) 2023  lil star

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published
by the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
"""

import os
import asyncio
from typing import Type, Union
from datetime import datetime, timedelta
from html import escape
from datetime import datetime
from io import BytesIO
import re

from PIL import Image
from maubot import Plugin, MessageEvent
from maubot.handlers import command
from mautrix.util.config import BaseProxyConfig
from mautrix.util.async_db import UpgradeTable
from mautrix.types import (
    UserID,
    RoomID,
    TextMessageEventContent,
    MessageType,
    Format,
    MediaMessageEventContent,
    ImageInfo,
)

import maumod.constants as constants
from maumod.config import Config
from maumod.database import TSDB, upgrade_table
import maumod.util as mutil
from maumod.tsapi import Completions, Chat, Translate, Compliment, Generate


class TextSynth(Plugin):

    user_chat_history: dict
    user_complete_history: dict
    cleanup_delta: int
    chat_history_length: int
    completer: Completions
    chatter: Chat
    translater: Translate
    generator: Generate
    name: str
    db: TSDB

    @classmethod
    def get_config_class(cls) -> Type[BaseProxyConfig]:
        return Config

    @classmethod
    def get_db_upgrade_table(cls) -> UpgradeTable:
        return upgrade_table

    async def start(self) -> None:
        self.on_external_config_update()
        api_key = os.getenv("TEXTSYNTH_API_KEY")
        if api_key is None:
            raise KeyError(
                f"Tried to get environment variable TEXTSYNTH_API_KEY, but could not find. This is required."
            )
        self.completer = Completions(self.http, api_key, self.log)
        self.chatter = Chat(self.http, api_key, self.log)
        self.translater = Translate(self.http, api_key, self.log)
        self.complimenter = Compliment(self.http, api_key, self.log)
        self.generator = Generate(self.http, api_key, self.log)
        self.name = (
            self.config["name"]
            or await self.client.get_displayname(self.client.mxid)
            or self.client.parse_user_id(self.client.mxid)[0]
        )
        self.log.debug(f"DEBUG gpt plugin started with bot name: {self.name}")
        self.db = TSDB(self.database, self.config, self.log)
        self.user_chat_history = {}
        self.user_complete_history = {}
        self.log.debug(f"DEBUG initialized sqlite database")
        self.clear_loop_task = asyncio.create_task(self.clear_loop())

    async def stop(self) -> None:
        self.clear_loop_task.cancel()

    def on_external_config_update(self):
        self.config.load_and_update()
        self.cleanup_delta = self.config["cleanup_delta"]
        self.chat_history_length = self.config["chat_history"]

    async def clear_user_history(
        self, user_id: str, summary: bool = False
    ) -> Union[str, None]:
        res = "Clear Summary:"
        chat_history = self.user_chat_history.pop(user_id, None)
        complete_history = self.user_complete_history.pop(user_id, None)
        if chat_history:
            res += "\n* Cleared your chat history."
        else:
            res += "\n* No chat history to clear."
        if complete_history:
            res += "\n* Cleared your complete history."
        else:
            res += "\n* No complete history to clear."
        if summary:
            return res

    async def cleanup_old_history(self, info_dict: dict) -> None:
        now = datetime.now()
        to_clean = []
        for k, v in info_dict:
            old: datetime = v["time"]
            if (now - old).total_seconds() > self.cleanup_delta:
                to_clean.append(k)
        for item in to_clean:
            del info_dict[item]

    async def clear_loop(self) -> None:
        try:
            self.log.debug("DEBUG Starting do stuff loop.")
            while True:
                subs = await self.db.get_subscriptions()
                now = datetime.now()
                self.cleanup_old_history(self.user_chat_history)
                self.cleanup_old_history(self.user_complete_history)
                for sub in subs:
                    user_id: str = sub[0]
                    room_id: str = sub[1]
                    sub_time: datetime = sub[2]
                    if now.hour == sub_time.hour and now.minute == sub_time.minute:
                        self.log.debug(
                            f"Dispatching compliment task for {user_id} in {room_id}"
                        )
                        await self.dispatch_compliment(user_id, room_id)
                next_minute = (now + timedelta(minutes=1)).replace(
                    second=0, microsecond=0
                )
                await asyncio.sleep((next_minute - now).total_seconds())
                self.log.debug("DEBUG Did stuff.")
        except asyncio.CancelledError:
            self.log.debug("DEBUG Cancelled do stuff loop.")
        except Exception:
            self.log.exception("ERROR Exception in do stuff loop.")

    async def dispatch_compliment(self, user_id: UserID, room_id: RoomID) -> None:
        message, _ = await self.complimenter.post(user_id)
        message = message.strip('"')
        user_id_at = f"@{user_id}"
        user_html = f"<a href='https://matrix.to/#/{user_id_at}'>{user_id_at}</a>"
        content = TextMessageEventContent(
            msgtype=MessageType.TEXT,
            body=f"{user_id_at}: {message}",
            format=Format.HTML,
            formatted_body=f"{user_html}: {escape(message)}",
        )
        await self.client.send_message(room_id, content)
        self.log.debug(f"Finished dispatching compliment to {user_id} in {room_id}")

    @command.new(
        name="tschat", help="Chat with your chosen model", require_subcommand=False
    )
    @command.argument("message", pass_raw=True, required=True)
    async def tschat(self, e: MessageEvent, message: str) -> None:
        await self.client.set_typing(e.room_id, timeout=99999)
        user_info = await self.db.get_user_config(e.sender)
        if e.sender not in self.user_chat_history.keys():
            messages = [message]
            self.user_chat_history[e.sender] = {
                "time": datetime.now(),
                "data": messages,
            }
        else:
            self.user_chat_history[e.sender]["time"] = datetime.now()
            self.user_chat_history[e.sender]["data"].append(message)
        curr_data = self.user_chat_history[e.sender]["data"]
        reply, _ = await self.chatter.post(user_info, curr_data)
        reply_text = f"> {curr_data[-1]}\n\n{reply}"
        curr_data.append(reply)
        if len(curr_data) > self.chat_history_length:
            curr_data = curr_data[2:]
        await self.client.set_typing(e.room_id, timeout=0)
        await e.respond(reply_text)

    @command.new(
        name="tscomp",
        help="Complete the text with your chosen model",
        require_subcommand=False,
    )
    @command.argument("message", pass_raw=True, required=True)
    async def complete(self, e: MessageEvent, message: str) -> None:
        await self.client.set_typing(e.room_id, timeout=99999)
        user_info = await self.db.get_user_config(e.sender)
        self.user_complete_history[e.sender] = {
            "time": datetime.now(),
            "data": message,
        }
        reply, _ = await self.completer.post(user_info, message)
        self.user_complete_history[e.sender]["data"] = reply
        await self.client.set_typing(e.room_id, timeout=0)
        await e.respond(reply)

    @command.new(
        name="trans", help="Translate text between languages", require_subcommand=False
    )
    @command.argument("from_lang", required=True)
    @command.argument("to_lang", required=True)
    @command.argument("message", pass_raw=True, required=True)
    async def trans(
        self, e: MessageEvent, from_lang: str, to_lang: str, message: str
    ) -> None:
        await self.client.set_typing(e.room_id, timeout=99999)
        from_lang_valid = mutil.check_language(from_lang)
        to_lang_valid = mutil.check_language(to_lang)
        bad_replies = [
            f"You provided from language code `{from_lang}`, which is not valid."
            f"You provided to language code `{to_lang}`, which is not valid."
            f"Please provide valid codes and try again."
        ]
        if not from_lang_valid and not to_lang_valid:
            reply_text = "\n".join(bad_replies)
        elif not from_lang_valid:
            reply_text = "\n".join([bad_replies[0], bad_replies[2]])
        elif not to_lang_valid:
            reply_text = "\n".join(bad_replies[1:])
        else:
            user = await self.db.get_user_config(e.sender)
            reply_text = await self.translater.post(user, from_lang, to_lang, message)
        await self.client.set_typing(e.room_id, timeout=0)
        await e.reply(reply_text)

    @command.new(
        name="tslang",
        help="List available translation languages.",
        require_subcommand=False,
    )
    async def translate_languages(self, e: MessageEvent) -> None:
        reply_text = await mutil.get_available_language_str()
        await e.reply(reply_text, in_thread=True)

    @command.new(name="tscont", help="Continue completion or expand on chat")
    async def continue_gen(self, e: MessageEvent):
        return

    @continue_gen.subcommand(
        name="complete", help="Continue previous completion of text"
    )
    async def continue_gen_complete(self, e: MessageEvent):
        if e.sender not in self.user_complete_history.keys():
            await e.reply("You do not have a current complete history")
            return
        await self.client.set_typing(e.room_id, timeout=99999)
        user_info = await self.db.get_user_config(e.sender)
        self.user_complete_history[e.sender]["time"] = datetime.now()
        curr_data = self.user_complete_history[e.sender]["data"]
        reply, _ = await self.completer.post(user_info, curr_data)
        self.user_complete_history[e.sender]["data"] = reply
        await self.client.set_typing(e.room_id, timeout=0)
        await e.respond(reply)

    @continue_gen.subcommand(name="chat", help="Continue generation of previous chat")
    async def continue_gen_chat(self, e: MessageEvent):
        if e.sender not in self.user_chat_history.keys():
            await e.reply("You have no chat history to use.")
            return
        await self.client.set_typing(e.room_id, timeout=99999)
        user_info = await self.db.get_user_config(e.sender)
        self.user_chat_history[e.sender]["time"] = datetime.now()
        curr_data = self.user_chat_history[e.sender]["data"]
        reply, _ = await self.chatter.post(user_info, curr_data)
        curr_data[-1] += reply
        reply_text = f"> {curr_data[-2]}\n\n{curr_data[-1]}"
        await self.client.set_typing(e.room_id, timeout=0)
        await e.respond(reply_text)

    @command.new(
        name="tsclear",
        help="Clear chat history for your user",
        require_subcommand=False,
    )
    async def tsclear(self, e: MessageEvent) -> None:
        await e.reply(await self.clear_user_history(e.sender, True))

    @command.new(
        name="tspurge",
        help="Purge your user from the database",
        require_subcommand=False,
    )
    async def tspurge(self, e: MessageEvent) -> None:
        await self.db.purge_user(e.sender)
        await e.react(constants.CHECK)

    @command.new(
        name="tshelp",
        help="Get help for the available commands",
        require_subcommand=False,
    )
    @command.argument("cmd", required=False)
    async def tshelp(self, e: MessageEvent, cmd: str):
        if cmd in constants.HELP_TEXTS.keys():
            reply_text = constants.HELP_TEXTS[cmd]
        elif cmd == "tshelp":
            await e.react("🧐")
            return
        else:
            reply_text = constants.HELP_SUMMARY
        await e.respond(reply_text)

    @command.new(
        name="tsconf",
        help="Get or set config values for your user",
        require_subcommand=False,
    )
    @command.argument("key", required=False)
    @command.argument("val", required=False)
    async def tsconfig(self, e: MessageEvent, key: str, val: str):
        user_info = await self.db.get_user_config(e.sender)
        if key and val:
            try:
                okay = False
                if key == "chat_engine" and val in constants.ENGINES_CHAT:
                    okay = True
                elif key == "completion_engine" and val in constants.ENGINES_COMPLETION:
                    okay = True
                elif key == "question_engine" and val in constants.ENGINES_QUESTION:
                    okay = True
                elif (
                    key == "translation_engine" and val in constants.ENGINES_TRANSLATION
                ):
                    okay = True
                elif key == "max_tokens" and int(val) in constants.MAX_TOKENS_RANGE:
                    okay = True
                    val = int(val)
                elif key == "top_k" and int(val) in constants.TOP_K_RANGE:
                    okay = True
                    val = int(val)
                elif key == "top_p" and int(val) in constants.TOP_P_RANGE:
                    okay = True
                    val = int(val)

                if okay:
                    await self.db.set_value(e.sender, key, val)
                    await e.react(constants.CHECK)
                else:
                    await e.react(constants.CROSS)
            except:
                await e.react(constants.CROSS)
        elif key:
            if key in constants.CONFIG_HELP_TEXTS.keys():
                await e.reply(constants.CONFIG_HELP_TEXTS[key])
            else:
                await e.react(constants.CROSS)
        else:
            await e.reply(str(user_info))

    @command.new(
        name="compliment",
        help="Subscribe or unsubscribe to a daily compliment.",
        require_subcommand=True,
    )
    async def compliment(self, e: MessageEvent):
        pass

    @compliment.subcommand(name="subscribe", help="Subscribe to a daily compliment.")
    async def compliment_subscribe(self, e: MessageEvent):
        res = await self.db.subscribe_user(e.sender[1:], e.room_id)
        await e.react(constants.CHECK)

    @compliment.subcommand(
        name="unsubscribe", help="Unsubscribe from a daily compliment."
    )
    async def compliment_unsubscribe(self, e: MessageEvent):
        res = await self.db.unsubscribe_user(e.sender[1:])
        await e.react(constants.CHECK)

    @command.new(name="tsgen", help="Generate stable diffusion images.")
    @command.argument(name="prompt", pass_raw=True, required=True)
    async def tsgen(
        self,
        e: MessageEvent,
        prompt: str,
    ):
        await self.client.set_typing(e.room_id, timeout=99999)
        count_match = re.search(r"count=(\d+)", prompt)
        if count_match is not None:
            image_count = int(count_match.group(1))
            prompt = prompt.replace(count_match.group(0), "")
        else:
            image_count = None
        timesteps_match = re.search(r"timesteps=(\d+)", prompt)
        if timesteps_match is not None:
            timesteps = int(timesteps_match.group(1))
            prompt = prompt.replace(timesteps_match.group(0), "")
        else:
            timesteps = None
        guidance_scale_match = re.search(
            r"guidance_scale=((\d+(\.\d+)?)|(\.\d+))", prompt
        )
        if guidance_scale_match is not None:
            guidance_scale = float(guidance_scale_match.group(1))
            prompt = prompt.replace(guidance_scale_match.group(0), "")
        else:
            guidance_scale = None
        if "neg=" in prompt:
            prompt, neg = prompt.split("neg=")
            neg = neg.strip()
        else:
            neg = None
        prompt = " ".join(x for x in prompt.split())
        self.log.debug(
            f"Generating image with count of {image_count}, timesteps of {timesteps}, guidance scale of {guidance_scale}\nPrompt: {prompt}\nNegative Prompt: {neg}"
        )
        response = await self.generator.post(
            image_count, timesteps, guidance_scale, prompt, neg
        )
        if response is None:
            await e.reply("Unspecified error from API")
            await self.client.set_typing(e.room_id, timeout=0)
            return
        mime_type = "image/jpeg"
        for image_data in response:
            filename = f"{datetime.now().timestamp()}.jpeg"
            uri = await self.client.upload_media(image_data, mime_type=mime_type)
            image = Image.open(BytesIO(image_data))
            size = len(image_data)
            width, height = image.size
            content = MediaMessageEventContent(
                url=uri,
                msgtype=MessageType.IMAGE,
                body=filename,
                info=ImageInfo(
                    mimetype=mime_type,
                    size=size,
                    width=width,
                    height=height,
                ),
            )
            await self.client.send_message(e.room_id, content)
        await self.client.set_typing(e.room_id, timeout=0)
