
from .constants import LANGUAGE_CODES

async def check_language(lang: str) -> bool:
    if lang in LANGUAGE_CODES.keys():
        return True
    return False

async def get_available_language_str() -> str:
    langs = '\n'.join([f'* **{k}**: {v}' for k, v in LANGUAGE_CODES.items()])
    return f'Available translation languages:\n{langs}'