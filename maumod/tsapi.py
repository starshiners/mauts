from typing import Tuple
import json
import random
import base64

from aiohttp import ClientSession
from mautrix.util.logging import TraceLogger
from mautrix.types import UserID

import maumod.constants as constants
from .database import UserInfo


class TSAPI:
    def __init__(self, http: ClientSession, key: str, log: TraceLogger) -> None:
        self.url = constants.TS_URL
        self.log = log
        self.session = http
        self.mode = None
        self.headers = {
            "Content-Type": "application/json",
            "Authorization": f"Bearer {key}",
        }


class Completions(TSAPI):
    def __init__(self, http: ClientSession, key: str, log: TraceLogger) -> None:
        super().__init__(http, key, log)
        self.mode = "completions"

    async def post(self, user: UserInfo, completion_text: str) -> Tuple[str, bool]:
        post_url = f"{self.url}/{user.completion_engine}/{self.mode}"
        post_data = {
            "max_tokens": user.max_tokens,
            "prompt": completion_text,
            "top_k": user.top_k,
            "top_p": round(user.top_p / 10, 1),
        }
        async with self.session.post(
            post_url, headers=self.headers, data=json.dumps(post_data)
        ) as response:
            if response.status != 200:
                return (
                    f"Reponse Code: {response.status}\nMessage: {await response.text()}",
                    True,
                )
            response_json = await response.json()
            text = response_json["text"]
            end = response_json["reached_end"]
            input_tokens = response_json["input_tokens"]
            output_tokens = response_json["output_tokens"]
            self.log.debug(
                f"DEBUG Used {input_tokens} input tokens and {output_tokens} tokens"
            )
            return f"{completion_text}{text}", end


class Chat(TSAPI):
    def __init__(self, http: ClientSession, key: str, log: TraceLogger) -> None:
        super().__init__(http, key, log)
        self.mode = "chat"

    async def post(self, user: UserInfo, chat_messages: list[str]) -> Tuple[str, bool]:
        post_url = f"{self.url}/{user.chat_engine}/{self.mode}"
        post_data = {
            "messages": chat_messages,
            "max_tokens": user.max_tokens,
            "top_k": user.top_k,
            "top_p": round(user.top_p / 10, 1),
        }
        async with self.session.post(
            post_url, headers=self.headers, data=json.dumps(post_data)
        ) as response:
            if response.status != 200:
                return (
                    f"Reponse Code: {response.status}\nMessage: {await response.text()}",
                    True,
                )
            response_json = await response.json()
            text = response_json["text"]
            end = response_json["reached_end"]
            input_tokens = response_json["input_tokens"]
            output_tokens = response_json["output_tokens"]
            self.log.debug(
                f"DEBUG Used {input_tokens} input tokens and {output_tokens} tokens"
            )
            return text, end


class Translate(TSAPI):
    def __init__(self, http: ClientSession, key: str, log: TraceLogger) -> None:
        super().__init__(http, key, log)
        self.mode = "translate"

    async def post(
        self, user: UserInfo, from_lang: str, to_lang: str, message: str
    ) -> str:
        post_url = f"{self.url}/{user.translation_engine}/{self.mode}"
        messages = [message]
        post_data = {"text": messages, "source_lang": from_lang, "target_lang": to_lang}
        async with self.session.post(
            post_url, headers=self.headers, data=json.dumps(post_data)
        ) as response:
            if response.status != 200:
                return (
                    f"Reponse Code: {response.status}\nMessage: {await response.text()}"
                )
            response_json = await response.json()
            self.log.debug(
                f"Response from api: {response_json}"
                f"request body: {post_data}"
                f"request body as string: {json.dumps(post_data)}"
            )
            translation = response_json["translations"][0]["text"]
            input_tokens = response_json["input_tokens"]
            output_tokens = response_json["output_tokens"]
            self.log.debug(
                f"DEBUG Used {input_tokens} input tokens and {output_tokens} tokens"
            )
            return translation


class Compliment(TSAPI):
    def __init__(self, http: ClientSession, key: str, log: TraceLogger) -> None:
        super().__init__(http, key, log)
        self.mode = "chat"

    async def post(self, user_id: str) -> str:
        post_url = f"{self.url}/{constants.ENGINES_CHAT[0]}/{self.mode}"
        adjective = random.choice(constants.ADJECTIVES)
        post_data = {
            "messages": [
                f"Generate a compliment for a person with the given name and keywords. \nName: {user_id}\nKeywords: {adjective}"
            ],
            "max_tokens": 200,
        }
        async with self.session.post(
            post_url, headers=self.headers, data=json.dumps(post_data)
        ) as response:
            if response.status != 200:
                return (
                    f"Reponse Code: {response.status}\nMessage: {await response.text()}",
                    True,
                )
            response_json = await response.json()
            text = response_json["text"]
            end = response_json["reached_end"]
            input_tokens = response_json["input_tokens"]
            output_tokens = response_json["output_tokens"]
            self.log.debug(
                f"DEBUG Used {input_tokens} input tokens and {output_tokens} tokens"
            )
            return text, end


class Generate(TSAPI):
    def __init__(self, http: ClientSession, key: str, log: TraceLogger) -> None:
        super().__init__(http, key, log)
        self.mode = "text_to_image"

    async def post(
        self,
        image_count: int | None,
        timesteps: int | None,
        guidance_scale: float | None,
        prompt: str,
        negative_prompt: str | None,
    ) -> list[bytes] | None:
        post_url = f"{self.url}/{constants.ENGINES_GENERATE[0]}/{self.mode}"
        post_data = {
            "prompt": prompt,
            "image_count": 1 if image_count is None else image_count,
            "width": 512,
            "height": 512,
            "timesteps": 50 if timesteps is None else timesteps,
            "guidance_scale": 7.5 if guidance_scale is None else guidance_scale,
            "negative_prompt": "" if negative_prompt is None else negative_prompt,
        }
        async with self.session.post(
            post_url, headers=self.headers, data=json.dumps(post_data)
        ) as response:
            if response.status not in [200, 201]:
                return (
                    f"Reponse Code: {response.status}\nMessage: {await response.text()}",
                    True,
                )
            response_json = await response.json()
            if "images" not in response_json:
                self.log.error("Unexpected response for text_to_image")
                return None
            return [base64.b64decode(x["data"]) for x in response_json["images"]]
