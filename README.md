# mauts

mauts is a plugin for [maubot](https://maubot.xyz/) which interacts with the [textsynth](https://textsynth.com/) API.

## Getting Started

You will need an API key from [textsynth](https://textsynth.com/). You will want to create an account and then purchase some credits. This key should go in an environment variable called `TEXTSYNTH_API_KEY`

Afterwards, you can clone this repo and run `mbc build -u`. Presuming you are logged in via `mbc login` this will build and upload the plugin. From there follow the regular directions to set it up in the admin interface.

mauts does not require any additional dependencies outside of what is needed to run maubot.

## Configuration

Values related to the textsynth API are described in their [documentation](https://textsynth.com/documentation.html), but `constants.py` includes the ones currently implemented by mauts. In the event they make updates or change out models you will need to edit this file.

The `base-config-yaml` file has several options, they are described below.

* cleanup_delta: How long in seconds between clearing user's chat histories.
* chat_history: How many chat messages can be retained/sent when using the `!tschat` command.
* chat_engine: Which chat engine to use by default
* completion_engine: Which completion engine to use by default
* question_engine: Which engine to use for answering questions
* translation_engine: Which engine to use for translation
* max_tokens: The max amount of tokens to generate (only affects completions)
* top_k: See textsynth docs (only affect completions)
* top_p: See textsynth docs (only affect completions). Note this value is divided by 10 internally.

## Usage

You can use the `!tshelp` command once the bot is up and running to get the available commands. Then use `!tshelp <command>` to get further information. Things should be fairly self explanatory.

## Privacy / Compliance

The bot's primary operation is to send and recieve data via textsynth's API. As such, when using any of these commands, you should take note of their [privacy policy](https://textsynth.com/privacy.html).

The textsynth API uses proprietary software called [ts_server](https://bellard.org/ts_server/). There is a free version also available for download, with limited functionality. Per their [pricing page](https://textsynth.com/pricing.html), they claim to not log API request content for paid users.

Note that using the `llamma2_7b` model has certain additional restrictions [imposed by meta](https://ai.meta.com/resources/models-and-libraries/llama-downloads/). If you do not agree with this then do not use it.

As for the bot itself:

* Chat / Completion history is stored in memory for a limited time per user.
* User configuration is stored in the postgres database, which includes your username.
* Unencrypted bot replies **are** logged by maubot if debug logging is turned on.

## License

mauts is licensed under the [GNU Affero General Public License](https://www.gnu.org/licenses/agpl-3.0.txt).

Thank you to the authors of the following projects, as I borrowed some bits and pieces:

* [reminder](https://github.com/maubot/reminder)
* [maubot-ntfy](https://gitlab.com/999eagle/maubot-ntfy)
* [tmdb-bot](https://codeberg.org/lomion/tmdb-bot)
